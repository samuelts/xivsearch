module.exports = {
  pathPrefix: `/xivsearch`,
  siteMetadata: {
    title: `xivsearch`,
    description: `A search interface for XIVAPI`,
    author: `@samuelts`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `xivsearch`,
        short_name: `xivsearch`,
        start_url: `/`,
        background_color: `#1a1f27`,
        theme_color: `#1a1f27`,
        display: `minimal-ui`,
        icon: `src/images/ffxiv-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
