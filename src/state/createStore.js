import { createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

import { actionTypes } from './actions';

// Global Constants
const API_KEY = 'bc2266571eed4f5dbdc28336';
export const API_INDICES = {
    "Core": {
        "Achievements": [false, "Achievement"],
        "Actions": [false, "Action"],
        "Craft Actions": [false, "CraftAction"],
        "Fates": [false, "Fate"],
        "Instances": [false, "InstanceContent"],
        "Items": [false, "Item"],
        "Leves": [false, "Leve"],
        "Quests": [false, "Quest"],
        "Recipes": [false, "Recipe"],
        "Traits": [false, "Trait"]
    },
    "Misc": {
        "Buddy Equips": [false, "BuddyEquip"],
        "Companions": [false, "Companion"],
        "Emotes": [false, "Emote"],
        "Mounts": [false, "Mount"],
        "NPCs": [false, "BNpcName,ENpcResident"],
        "Orchestrion": [false, "Orchestrion"],
        "Places": [false, "Place"],
        "Status": [false, "Status"],
        "Titles": [false, "Title"],
        "Weather": [false, "Weather"],
        "World": [false, "World"]
    }
}

const initialState = {
    apiKey: API_KEY,
    term: "",
    results: [],
    lodestone: {},
    worldStatus: [],
    language: "en",
    limit: "100",
    itemList: null,
    newsList: null,
    pagination: {},
    indices: API_INDICES,
    indexStr: "",
    isCharSearch: true,
    lastItem: {},
    lastChar: {},
    homeLoading: false,
    searchLoading: false,
    getLoading: false,
    error: null
}

// REDUCERS
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LANGUAGE_SELECT:
            return Object.assign({}, state, {
                language: action.language
            })
        case actionTypes.UPDATE_INDICES:
            return Object.assign({}, state, {
                indices: action.indices ? action.indices : null,
                indexStr: action.indexStr
            })
        case actionTypes.UPDATE_TERM:
            return Object.assign({}, state, {
                term: action.term
            })
        case actionTypes.UPDATE_CURRENT:
            return Object.assign({}, state, {
                currItem: action.item
            })
        case actionTypes.LIMIT_SELECT:
            return Object.assign({}, state, {
                limit: action.limit
            })
        case actionTypes.CLEAR_LIST:
            return Object.assign({}, state, {
                itemList: null
            })
        case actionTypes.SET_LIST:
            return Object.assign({}, state, {
                itemList: action.itemList
            })
            // SEARCH
        case actionTypes.XIVSEARCH_PENDING:
            return Object.assign({}, state, {
                searchLoading: true
            })
        case actionTypes.XIVSEARCH_FULFILLED:
            return Object.assign({}, state, {
                results: action.payload.data.Results,
                pagination: action.payload.data.Pagination,
                searchLoading: false
            })
        case actionTypes.XIVSEARCH_REJECTED:
            return Object.assign({}, state, {
                error: action.error,
                searchLoading: false
            })
            // GET ITEM
        case actionTypes.XIVGET_ITEM_PENDING:
            return Object.assign({}, state, {
                getLoading: true
            })
        case actionTypes.XIVGET_ITEM_FULFILLED:
            return Object.assign({}, state, {
                lastItem: action.payload.data,
                getLoading: false
            })
        case actionTypes.XIVGET_ITEM_REJECTED:
            return Object.assign({}, state, {
                error: action.error,
                getLoading: false
            })
            // GET NEWS
        case actionTypes.XIVGET_LODESTONE_PENDING:
            return Object.assign({}, state, {
                homeLoading: true
            })
        case actionTypes.XIVGET_LODESTONE_FULFILLED:
            return Object.assign({}, state, {
                lodestone: action.payload.data,
                homeLoading: false
            })
        case actionTypes.XIVGET_LODESTONE_REJECTED:
            return Object.assign({}, state, {
                error: action.error,
                homeLoading: false
            })  
            // GET WORLD STATUS
        case actionTypes.XIVGET_WORLDSTAT_PENDING:
            return Object.assign({}, state, {
                homeLoading: true
            })
        case actionTypes.XIVGET_WORLDSTAT_FULFILLED:
            return Object.assign({}, state, {
                worldStatus: action.payload.data,
                homeLoading: false
            })
        case actionTypes.XIVGET_WORLDSTAT_REJECTED:
            return Object.assign({}, state, {
                error: action.error,
                homeLoading: false
            })
        case actionTypes.SET_CHAR_SEARCH:
            return Object.assign({}, state, {
                isCharSearch: action.isCharSearch
            })
        case actionTypes.XIVGET_CHARACTER_PENDING:
            return Object.assign({}, state, {
                getLoading: true
            })
        case actionTypes.XIVGET_CHARACTER_FULFILLED:
            return Object.assign({}, state, {
                lastChar: action.payload.data,
                getLoading: false
            })
        case actionTypes.XIVGET_CHARACTER_REJECTED:
            return Object.assign({}, state, {
                error: action.error,
                getLoading: false
            })
        default:
            return state;
    }
}

export default function configureStore(initialState){
    const windowExist = typeof window === 'object';

    const store = createStore(
        reducer,
        initialState,
        compose(
            applyMiddleware(thunk, promiseMiddleware()),
            windowExist && window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
        )
    );

    return store;
}

export const store = configureStore()