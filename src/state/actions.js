import React from 'react';
import axios from 'axios';
import { navigate } from 'gatsby';
import Item from '../components/search/item';
import { store } from './createStore';

// #region Non-redux exports 
export const actionTypes = {
    XIVSEARCH: "XIVSEARCH",
    XIVSEARCH_PENDING: "XIVSEARCH_PENDING",
    XIVSEARCH_FULFILLED: "XIVSEARCH_FULFILLED",
    XIVSEARCH_REJECTED: "XIVSEARCH_REJECTED",
    XIVGET_ITEM: "XIVGET_ITEM",
    XIVGET_ITEM_PENDING: "XIVGET_ITEM_PENDING",
    XIVGET_ITEM_FULFILLED: "XIVGET_ITEM_FULFILLED",
    XIVGET_ITEM_REJECTED: "XIVGET_ITEM_REJECTED",
    XIVGET_LODESTONE: "XIVGET_LODESTONE",
    XIVGET_NEWS_PENDING: "XIVGET_LODESTONE_PENDING",
    XIVGET_LODESTONE_FULFILLED: "XIVGET_LODESTONE_FULFILLED",
    XIVGET_LODESTONE_REJECTED: "XIVGET_LODESTONE_REJECTED",
    XIVGET_WORLDSTAT: "XIVGET_WORLDSTAT",
    XIVGET_WORLDSTAT_PENDING: "XIVGET_WORLDSTAT_PENDING",
    XIVGET_WORLDSTAT_FULFILLED: "XIVGET_WORLDSTAT_FULFILLED",
    XIVGET_WORLDSTAT_REJECTED: "XIVGET_WORLDSTAT_REJECTED",
    XIVGET_CHARACTER: "XIVGET_CHARACTER",
    XIVGET_CHARACTER_PENDING: "XIVGET_CHARACTER_PENDING",
    XIVGET_CHARACTER_FULFILLED: "XIVGET_CHARACTER_FULFILLED",
    XIVGET_CHARACTER_REJECTED: "XIVGET_CHARACTER_REJECTED",
    LANGUAGE_SELECT: "LANGUAGE_SELECT",
    LIMIT_SELECT: "LIMIT_SELECT",
    UPDATE_INDICES: "UPDATE_INDICES",
    UPDATE_TERM: "UPDATE_TERM",
    UPDATE_CURRENT: "UPDATE_CURRENT",
    CLEAR_LIST: "CLEAR_LIST",
    SET_LIST: "SET_LIST",
    SET_CHAR_SEARCH: "SET_CHAR_SEARCH"
}

export function buildUrl(location=null, page=null) {
    let { isCharSearch, term, apiKey, indexStr, language, limit } = store.getState();

    let API_URL, navigatePath;
    if (isCharSearch) {
        API_URL = `https://xivapi.com/character/search?name=${term}`+
        `${page ? `&page=${page}` : ""}`+
        `&key=${apiKey}`;

        navigatePath = `search${term ? `?name=${term}` : ""}`+
        `${page ? `&page=${page}` : ""}`;

    } else {
        API_URL = `https://xivapi.com/search?string=${term}`+
        `${indexStr ? `&indexes=${indexStr}`: ""}`+
        `&language=${language}`+
        `${page ? `&page=${page}` : ""}`+
        `&limit=${limit}`+
        `&key=${apiKey}`;

        navigatePath = `search${term ? `?string=${term}`: ""}`+
        `${indexStr ? `&indexes=${indexStr}`: ""}`+
        `${language ? `&language=${language}`: ""}`+
        `${page ? `&page=${page}` : ""}`+
        `${limit ? `&limit=${limit}` : ""}`;

    }

    console.log("In buildUrl(), navigatePath=", navigatePath);

    if (location !== navigatePath && term !== "") {
        navigate(navigatePath);
        console.log("Should have navigated.");
    }

    return(API_URL);
}
// #endregion

// #region Redux action creators
export function xivSearch(url) {
    if (store.getState().term === "") {
        return dispatch => 
            dispatch({
                type: actionTypes.XIVSEARCH_REJECTED,
                payload: null
            })
    };
    return dispatch =>
        dispatch({
            type: actionTypes.XIVSEARCH,
            payload: axios.get(url)
        })
        .then(() => {
            const { isCharSearch, results } = store.getState();
            const itemList = results.map((item) => {
                if (isCharSearch) {
                    return (
                        <div key={`char:${item.ID}:wr`}>
                            <Item key={`char:${item.ID}`} item={item}/>
                            <hr key={`char:${item.ID}:hr`}/>
                        </div>
                    );
                } else {
                    return (
                        <div key={`${item.UrlType}:${item.ID}:wr`}>
                            <Item key={`${item.UrlType}:${item.ID}`} item={item}/>
                            <hr key={`${item.UrlType}:${item.ID}:hr`}/>
                        </div>
                    );
                }
            });
            dispatch(setList(itemList));
        })
}

export function xivGet(url, type) {
    const { apiKey, language, isCharSearch } = store.getState();
    const API_URL = isCharSearch 
        ? `https://xivapi.com${url}?key=${apiKey}`
        : `https://xivapi.com${url}?language=${language}&key=${apiKey}`;

    return dispatch =>
        dispatch({ 
            type: type,
            payload: axios.get(API_URL)
        })
}

export function onLanguageSelect(language) {
    return dispatch =>
        new Promise((resolve, reject) => {
            dispatch({ 
                type: actionTypes.LANGUAGE_SELECT,
                language: language
            });
        
            resolve();
        })
}

export function onLimitSelect(limit) {
    return dispatch =>
        new Promise((resolve) => {
            dispatch({
                type: actionTypes.LIMIT_SELECT,
                limit: limit
            });
            resolve();
        })
}

export function updateIndices(str, indices) {
    return (dispatch) =>
        new Promise((resolve) => {
            dispatch({ 
                type: actionTypes.UPDATE_INDICES,
                indexStr: str,
                indices: indices
            });
            resolve();
        })
}

export function updateTerm(term) {
    return dispatch => 
        dispatch({
            type: actionTypes.UPDATE_TERM,
            term: term
        })
}

export function clearList() {
    return dispatch =>
        dispatch({
            type: actionTypes.CLEAR_LIST
        })
}

export function setList(itemList) {
    return dispatch =>
        dispatch({
            type: actionTypes.SET_LIST,
            itemList: itemList
        })
}

export function charSearch(isCharSearch) {
    return dispatch =>
        dispatch({
            type: actionTypes.SET_CHAR_SEARCH,
            isCharSearch: isCharSearch
        })
}
// #endregion