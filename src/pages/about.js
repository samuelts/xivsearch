import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'

const IndexPage = () => (
  	<div id="about">
	    <SEO title="Home" keywords={[`gatsby`, `application`, `react`, `ffxiv`, `xiv`, `final`, `fantasy`, `game`, `search`, `mmo`, `mmorpg`]} />
	    <Layout>
	      	<div id="about_placeholder">About Page will be here</div>
	    </Layout>
  	</div>
)

export default IndexPage
