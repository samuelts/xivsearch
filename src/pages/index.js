import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'
import Home from '../components/home/home';

const IndexPage = () => (
  	<div id="home">
	    <SEO title="Home" keywords={[`gatsby`, `application`, `react`, `ffxiv`, `xiv`, `final`, `fantasy`, `game`, `search`, `mmo`, `mmorpg`]} />
	    <Layout>
	      	<Home />
	    </Layout>
  	</div>
)

export default IndexPage
