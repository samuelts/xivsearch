import React from 'react'
import { Location } from '@reach/router'
import Layout from '../components/layout'
import SEO from '../components/seo'
import SearchApp from '../components/search/search_app'

const IndexPage = () => (
  	<div id="search">
	    <SEO title="Home" keywords={[`gatsby`, `application`, `react`, `ffxiv`, `xiv`, `final`, `fantasy`, `game`, `search`, `mmo`, `mmorpg`]} />
	    <Location>
		    {({ location }) => (
		    	<Layout location={location}>
		      		<SearchApp />
		    	</Layout>
	    	)}
	    </Location>
  	</div>
)

export default IndexPage
