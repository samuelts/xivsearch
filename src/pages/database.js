import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'

const IndexPage = () => (
  	<div id="database">
	    <SEO title="Home" keywords={[`gatsby`, `application`, `react`, `ffxiv`, `xiv`, `final`, `fantasy`, `game`, `search`, `mmo`, `mmorpg`]} />
	    <Layout>
	      	<div id="db_placeholder">Database Page will be here</div>
	    </Layout>
  	</div>
)

export default IndexPage
