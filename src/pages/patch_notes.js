import React from 'react';

import Layout from '../components/layout';
import SEO from '../components/seo';
import PatchApp from '../components/patch_notes/patch_app';

const IndexPage = () => (
  	<div id="patch_notes">
	    <SEO title="Home" keywords={[`gatsby`, `application`, `react`, `ffxiv`, `xiv`, `final`, `fantasy`, `game`, `search`, `mmo`, `mmorpg`]} />
	    <Layout>
	      	<PatchApp />
	    </Layout>
  	</div>
)

export default IndexPage;
