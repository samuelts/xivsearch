import React from 'react'
import { connect } from 'react-redux';
import Header from './header'
import Footer from './footer'

class Layout extends React.Component {
    
   render() {
       return (
           <div className="layout">
                <Header location={this.props.location} />
                {this.props.children}
                <Footer />
           </div>
       )
   }
}

const mapStateToProps = state => {
    return { ...state }
}
  
export default connect(mapStateToProps)(Layout)