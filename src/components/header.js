import React, { Component } from 'react';
import { Link, navigate } from 'gatsby';
import queryString from 'query-string';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Button,
    UncontrolledTooltip,
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    CustomInput } from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp, faChevronDown, faFilter, faLanguage, faQuestion } from '@fortawesome/free-solid-svg-icons';
import * as actions from '../state/actions';
import SearchBar from './search/search_bar';
import { API_INDICES } from '../state/createStore';
import xiv_search from '../images/xiv_search.svg';

library.add(faChevronUp, faChevronDown, faFilter, faLanguage, faQuestion);

class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            navOpen: false,
            searchDropdownOpen: false,
            filterOpen: false,
            langOpen: false,
            oldIndexStr: "",
            oldIndices: API_INDICES
        }

        this.onIndexSelect = this.onIndexSelect.bind(this);
        this.onCharIndexSelect = this.onCharIndexSelect.bind(this);
        this.invertIndices = this.invertIndices.bind(this);
        this.clearIndices = this.clearIndices.bind(this);
        this.onSearchTermChange = this.onSearchTermChange.bind(this);
        this.onLanguageSelect = this.onLanguageSelect.bind(this);
        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.openSearchDropdown = this.openSearchDropdown.bind(this);
        this.closeSearchDropdown = this.closeSearchDropdown.bind(this);
        this.toggleFilterCard = this.toggleFilterCard.bind(this);
        this.toggleLang = this.toggleLang.bind(this);
        this.createIndexList = this.createIndexList.bind(this);
    }

    componentDidMount() {

        // Fetch results based on query string in url
        console.log("Header mounted, before parsing url");
        if (this.props.location) {
            const parsedParams = queryString.parse(this.props.location.search);
            let parsedIndices = API_INDICES;

            console.log("Parsed url: ", parsedParams);

            let term = "";
            if (parsedParams.string) {
                term = parsedParams.string;
                this.props.charSearch(false);
            } else if (parsedParams.name) {
                term = parsedParams.name;
                this.props.charSearch(true);
            }
            
            if (parsedParams.indexes) {
                const indexes = parsedParams.indexes.split(",");
                
                // Set index value to true if found in url
                indexes.forEach((i) => {
                    console.log(Object.values(parsedIndices.Core));

                    Object.entries(parsedIndices.Core).forEach((j) => {
                        if (j[1][1] === i) {
                            parsedIndices.Core[j[0]][0] = true;
                        }
                    })

                    Object.entries(parsedIndices.Misc).forEach((j) => {
                        if (j[1][1] === i) {
                            parsedIndices.Misc[j[0]][0] = true;
                        }
                    })
                });
            }

            console.log("Indices created.");

            console.log("Set limit", parsedParams.limit);
            this.props.onLimitSelect(parsedParams.limit ? parsedParams.limit : 100)
            .then(() => {
                console.log("Set language", parsedParams.language);
                this.props.onLanguageSelect(parsedParams.language ? parsedParams.language : "en")
                .then(() => {
                    console.log("Set indices", parsedParams.indexes, parsedIndices);
                    this.props.updateIndices(parsedParams.indexes ? parsedParams.indexes : null, parsedIndices)
                    .then(() => {
                        console.log("Set term", term);
                        this.props.updateTerm(term);
                        let api_url = actions.buildUrl(parsedParams.page);
                        this.props.xivSearch(api_url);
                    });
                });
            });
        }
    }

    onIndexSelect(group, index, checked) {
        const { indices } = this.props;
        let coreIndices, miscIndices;
        let indexStr = "";

        if (group === "Core") {
            coreIndices = {...indices.Core, [index]: [checked, indices.Core[index][1]]};
            miscIndices = indices.Misc;
        } else if (group === "Misc") {
            coreIndices = indices.Core;
            miscIndices = {...indices.Misc, [index]: [checked, indices.Misc[index][1]]};
        }
        
        const newIndices = {"Core": coreIndices, "Misc": miscIndices};

        for (let i in coreIndices) {
            if (coreIndices[i][0] === true) {
                indexStr+=`${indices.Core[i][1]},`;
            }
        }
        for (let i in miscIndices) {
            if (miscIndices[i][0] === true) {
                indexStr+=`${indices.Misc[i][1]},`;
            }
        }
        this.props.updateIndices(indexStr, newIndices)
        .then(() => {
            this.props.clearList();
            let api_url = actions.buildUrl();
            this.props.xivSearch(api_url);
        });
    }

    onCharIndexSelect() {
        if (this.props.isCharSearch) {
            this.props.updateIndices(this.state.oldIndexStr, this.state.oldIndices)
            .then(() => {
                this.props.clearList();
                this.props.charSearch(!this.props.isCharSearch)
                let api_url = actions.buildUrl();
                this.props.xivSearch(api_url);
            });
        } else {
            this.setState({
                oldIndexStr: this.props.indexStr,
                oldIndices: this.props.indices
            });
            this.props.updateIndices("", API_INDICES)
            .then(() => {
                this.props.clearList();
                this.props.charSearch(!this.props.isCharSearch)
                let api_url = actions.buildUrl();
                this.props.xivSearch(api_url);
            });
        }
    }

    invertIndices() {
        const { indices } = this.props;
        let newIndices = indices;
        let indexStr = "";
        
        for (let i in indices.Core) {
            newIndices.Core[i][0] = !indices.Core[i][0];
            if (newIndices.Core[i][0] === true) {
                indexStr+=`${newIndices.Core[i][1]},`;
            }
        }
        for (let i in indices.Misc) {
            newIndices.Misc[i][0] = !indices.Misc[i][0];
            if (newIndices.Misc[i][0] === true) {
                indexStr+=`${newIndices.Misc[i][1]},`;
            }
        }
        this.props.updateIndices(indexStr, newIndices)
        .then(() => {
            this.props.clearList();
            let api_url = actions.buildUrl();
            this.props.xivSearch(api_url);
        });
    }

    clearIndices() {
        const { indices } = this.props;
        let newIndices = indices;
        let indexStr = "";
        
        for (let i in indices.Core) {
            newIndices.Core[i][0] = false;
        }
        for (let i in indices.Misc) {
            newIndices.Misc[i][0] = false;
        }
        
        this.props.updateIndices(indexStr, newIndices)
        .then(() => {
            this.props.clearList();
            let api_url = actions.buildUrl();
            this.props.xivSearch(api_url);
        });
    }

    onSearchTermChange(term) {
        this.props.updateTerm(term);
        let api_url = actions.buildUrl();
        this.props.xivSearch(api_url);
    }

    onLanguageSelect(e) {
        this.props.onLanguageSelect(e.target.value)
        .then(() => {
            this.props.clearList();
            let api_url = actions.buildUrl();
            this.props.xivSearch(api_url);
        });
    }

    toggleNavbar() {
        this.setState({
            navOpen: !this.state.navOpen,
            filterOpen: false
        });
    }

    openSearchDropdown() {
        this.setState({
            searchDropdownOpen: true
        });
    }

    closeSearchDropdown() {
        this.setState({
            searchDropdownOpen: false
        });
    }

    toggleFilterCard() {
        this.setState({
            filterOpen: !this.state.filterOpen
        });
    }

    toggleLang() {
        this.setState({
            langOpen: !this.state.langOpen
        });
    }

    createIndexList() {
        const { indices } = this.props;
        // Core IndexList
        const coreIndexSwitches = Object.keys(indices.Core).map((index) => {
            return(
                <CustomInput
                    key={index}
                    type="switch"
                    name="customSwitch"
                    className="search-index"
                    id={`${index.toLowerCase()}_sw`}
                    value={index}
                    checked={indices.Core[index][0]}
                    onChange={() => this.onIndexSelect("Core", index, !indices.Core[index][0])}
                    label={`  ${index}`}
                />
            )
        });
        const miscIndexSwitches = Object.keys(indices.Misc).map((index) => {
            return(
                <CustomInput
                    key={index}
                    type="switch"
                    name="customSwitch"
                    className="search-index"
                    id={`${index.toLowerCase()}_sw`}
                    value={index}
                    checked={indices.Misc[index][0]}
                    onChange={() => this.onIndexSelect("Misc", index, !indices.Misc[index][0])}
                    label={`  ${index}`}
                />
            )
        });

        return [coreIndexSwitches, miscIndexSwitches]
    }

    render () {
        const { language } = this.props;
        const indexList = this.createIndexList();
        const coreIndexList = indexList[0];
        const miscIndexList = indexList[1];

        return (
            <div className="header" onMouseLeave={this.closeSearchDropdown}>
                <Navbar className="header-nav" expand="lg" dark>  
                    <NavbarBrand href="#"><img className="logo" src={xiv_search} alt="xiv search logo"/></NavbarBrand>
                    <NavbarToggler onClick={this.toggleNavbar} className="mr-2"/>
                    <Collapse isOpen={this.state.navOpen} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <Link to="" className="nav-link">Home</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/search" className="nav-link">Search</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/patch_notes" className="nav-link">Patch Notes</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/database" className="nav-link">Database</Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/about" className="nav-link">About</Link>
                            </NavItem>
                        </Nav>
                        <InputGroup id="searchbar-form">
                            <SearchBar value={this.props.term} onSearchTermChange={_.debounce((term) => this.onSearchTermChange(term), 500)} />
                            <InputGroupAddon addonType="append">
                                <Button color="dalamud" onClick={this.toggleFilterCard}>
                                    <FontAwesomeIcon icon="filter" />
                                </Button>
                            </InputGroupAddon>
                            <InputGroupButtonDropdown addonType="append" isOpen={this.state.langOpen} toggle={this.toggleLang}>
                                <DropdownToggle color="dalamud" size="sm" caret>
                                    {language.toUpperCase()}
                                </DropdownToggle>
                                <DropdownMenu className="bg-dalamud" id="lang-menu">
                                    <DropdownItem className="bg-dalamud text-light" value="en" onClick={this.onLanguageSelect}>EN</DropdownItem>
                                    <DropdownItem className="bg-dalamud text-light" value="de" onClick={this.onLanguageSelect}>DE</DropdownItem>
                                    <DropdownItem className="bg-dalamud text-light" value="fr" onClick={this.onLanguageSelect}>FR</DropdownItem>
                                    <DropdownItem className="bg-dalamud text-light" value="ja" onClick={this.onLanguageSelect}>JA</DropdownItem>
                                </DropdownMenu>
                            </InputGroupButtonDropdown>
                        </InputGroup>
                    </Collapse>
                </Navbar>
                
                <UncontrolledTooltip placement="bottom" target="filter-help">
                    Filters:<br/>
                    All Unselected: No filtering<br/>
                    Select a filter to restrict your results.<br/>
                    Filters are additive.
                </UncontrolledTooltip>

                <Collapse isOpen={this.state.filterOpen}>
                    <div id="filter-card">
                        <div id="filter-title">
                            Filters
                            <Button outline size="sm" color="millioncorn" id="filter-inv" onClick={this.invertIndices}>Invert</Button>
                            <Button outline size="sm" color="dalamud" id="filter-clear" onClick={this.clearIndices}>Clear</Button>
                            <Button outline size="sm" color="info" id="filter-help">
                                <FontAwesomeIcon icon="question" />
                            </Button>
                        </div>
                        <div id="char-index">
                            <div id="char-cat">
                            
                            </div>
                        </div>
                        <div id="unique-indices">
                            <div id="unqiue-cat">Unique: </div>
                            <CustomInput
                                type="switch"
                                name="customSwitch"
                                className="unique-index"
                                id="char-switch"
                                value="char"
                                checked={this.props.isCharSearch}
                                onChange={() => this.onCharIndexSelect()}
                                label="Character"
                            />
                        </div>
                        <div id="core-indices">
                            <div id="core-cat">Core: </div>
                            <div id="core-switches">{coreIndexList}</div>
                        </div>
                        <div id="misc-indices">
                            <div id="misc-cat">Misc: </div>
                            <div id="misc-switches">{miscIndexList}</div>
                        </div> 
                    </div>
                </Collapse>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return { ...state }
}

export default connect(mapStateToProps, actions)(Header)