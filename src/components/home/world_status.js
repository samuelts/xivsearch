import React from 'react';

const WorldStatus = (props) => {
    return (
        <div className="section-wrap" id="world-list">
            <h3 className="device-desktop">World Status</h3>
            {props.worldList}
        </div>
    );
}

export default WorldStatus;