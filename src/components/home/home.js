import React from 'react';
import { connect } from 'react-redux';
import { Card, 
    CardImg, 
    CardBody, 
    CardTitle, 
    TabContent, 
    TabPane, 
    Nav, 
    NavItem, 
    NavLink } from 'reactstrap';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import Moment from 'react-moment';
import WorldStatus from './world_status';
import News from './news';
import Notices from './notices';
import * as actions from '../../state/actions';
import { actionTypes } from '../../state/actions';

class Home extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            newsList: null,
            noticeList: null,
            worldList: null,
            activeTab: 'NewsList',
            newHtml: null
        }

        this.fixLinks = this.fixLinks.bind(this);
    }

    fixLinks(html) {
        const newHtml = html.replace('href="/lodestone/', 'href="https://na.finalfantasyxiv.com/lodestone/');
        return newHtml;
    }

    componentDidMount() {
        this.props.xivGet("/lodestone", actionTypes.XIVGET_LODESTONE).then(() => {
            this.props.xivGet("/lodestone/worldstatus", actionTypes.XIVGET_WORLDSTAT).then(() => {
                this.setState({
                    newsList: this.props.lodestone.News.map((i) => {
                        const newHtml = this.fixLinks(i.Html);
                        return (
                          <Card className="news-item" key={`${i.Time}:${i.Title}`}>
                            <CardImg src={i.Banner} />
                            <CardBody>
                                <CardTitle>
                                    <div>{i.Title}</div>
                                    <Moment parse="X" format="MM/DD/YYYY">{i.Time}</Moment>
                                </CardTitle>
                                <hr/>
                                {ReactHtmlParser(newHtml)}
                            </CardBody>
                          </Card>
                        );
                    }),
                    noticeList: this.props.lodestone.Notices.map((i) => {
                        return (
                            <Card className="notice-item" key={`${i.Time}:${i.Title}`}>
                                <CardBody>
                                    <CardTitle>
                                        <a href={i.Url}>{i.Title}</a>
                                        <Moment parse="X" format="MM/DD/YYYY">{i.Time}</Moment>
                                    </CardTitle>
                                </CardBody>
                          </Card>
                        );
                    }),
                    worldList: this.props.worldStatus.map((i) => {
                        const worldIcon = i.Status === "Online" ? <img className="world-icon" src="https://i.imgur.com/2S89IoZ.png" title="Online" alt="Online"/> 
                        : <img className="world-icon" src="https://i.imgur.com/Z03GliV.png" alt="Offline" />
                        return (
                            <div className="world-item" id={i.Title} key={i.Title}>
                                {worldIcon}
                                <div className="world-title">{i.Title}</div>
                            </div>
                        );
                    })
                })
            })
        });
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    } 

  render() {
    const { newsList, noticeList, worldList } = this.state;
    return(
        <React.Fragment>
            <div className="device-mobile page-wrap home-wrap" id="mobile-home">
                <Nav tabs>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === 'WorldList' })}
                            onClick={() => { this.toggle('WorldList'); }}
                        >
                            Worlds
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === 'NewsList' })}
                            onClick={() => { this.toggle('NewsList'); }}
                        >
                            News
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === 'NoticeList' })}
                            onClick={() => { this.toggle('NoticeList'); }}
                        >
                            Notices
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="WorldList">
                        <WorldStatus worldList={worldList} lodestone={this.props.lodestone}/>
                    </TabPane>
                    <TabPane tabId="NewsList">
                        <News newsList={newsList} homeLoading={this.props.homeLoading} lodestone={this.props.lodestone}/>
                    </TabPane>
                    <TabPane tabId="NoticeList">
                        <Notices noticeList={noticeList} lodestone={this.props.lodestone}/>
                    </TabPane>
                </TabContent>
            </div>
            <div className="device-desktop page-wrap home-wrap" id="desktop-home">
                <WorldStatus worldList={worldList} lodestone={this.props.lodestone}/>
                <News newsList={newsList} homeLoading={this.props.homeLoading} lodestone={this.props.lodestone}/>
                <Notices noticeList={noticeList} lodestone={this.props.lodestone}/>
            </div>
        </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
    return { ...state }
}
  
export default connect(mapStateToProps, actions)(Home)