import React from 'react';

const Notices = (props) => {
    return (
        <div className="section-wrap" id="notice-list">
            <h3 className="device-desktop">Notices</h3>
            {props.noticeList}
        </div>
    );
}

export default Notices;