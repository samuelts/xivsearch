import React from 'react';

const News = (props) => {
    return (
        <div className="section-wrap" id="news-list">
            <h3 className="device-desktop">News</h3>
            {props.newsList}
            <div className="lds-css-home ng-scope" style={props.homeLoading || !props.lodestone.Generated ? {display: "block"} : {display: "none"}}>
                <div className="lds-eclipse">
                    <div></div>
                </div>
            </div>
        </div>
    );
}

export default News;