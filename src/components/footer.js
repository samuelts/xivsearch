import React from 'react'

const Footer = () => {
    return(
        <div className="footer" id="footer"><div id="copyright">Safwyl - ©2019</div><div id="sqenix">All Final Fantasy XIV Content is Property of Square Enix Co., LTD</div></div>
    )
}

export default Footer