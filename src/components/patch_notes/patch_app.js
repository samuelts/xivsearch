import React from 'react';
import { connect } from 'react-redux';
import { TabContent, 
    TabPane, 
    Nav, 
    NavItem, 
    NavLink, 
    Card, 
    CardBody } from 'reactstrap';
import classnames from 'classnames';
import * as actions from '../../state/actions';

class PatchApp extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
          activeTab: 'Stormblood'
        };

        this.toggle = this.toggle.bind(this);
    }
    
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div className="page-wrap" id="patch_notes">
                <Nav tabs>
                    <NavItem>
                        <NavLink
                        className={classnames({ active: this.state.activeTab === 'Stormblood' })}
                        onClick={() => { this.toggle('Stormblood'); }}
                        >
                        Stormblood - 4.0
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                        className={classnames({ active: this.state.activeTab === 'Heavensward' })}
                        onClick={() => { this.toggle('Heavensward'); }}
                        >
                        Heavensward - 3.0
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                        className={classnames({ active: this.state.activeTab === 'ARR' })}
                        onClick={() => { this.toggle('ARR'); }}
                        >
                        A Realm Reborn - 2.0
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId='Stormblood'>
                        <img src="https://img.finalfantasyxiv.com/lds/h/H/zB-YYp8BN78u99OjjR0rI66_pk.png" alt="Stormblood banner"/>
                        <div className="patch-list">
                            <Card>
                                <CardBody>
                                    Patch Here
                                </CardBody>
                            </Card>
                        </div>
                    </TabPane>
                    <TabPane tabId='Heavensward'>
                        <img src="https://img.finalfantasyxiv.com/lds/h/O/U5TufT2QVnzXONyLf61X468dFU.png" alt="Heavensward banner" />
                        <div className="patch-list">
                            <Card>
                                <CardBody>
                                    Patch Here
                                </CardBody>
                            </Card>
                        </div>
                    </TabPane>
                    <TabPane tabId="ARR">
                        <img src="https://i.imgur.com/ZUUtGzH.png" alt="A Realm Reborn banner"/>
                        <div className="patch-list">
                            <Card>
                                <CardBody>
                                    Patch Here
                                </CardBody>
                            </Card>
                        </div>
                    </TabPane>
                </TabContent>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { ...state }
}
  
export default connect(mapStateToProps, actions)(PatchApp)