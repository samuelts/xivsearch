import React from 'react';
import { connect } from 'react-redux';
import { Input } from 'reactstrap';

const SearchBar = (props) => {
  return (
    <Input
        className="form-control border-jet bg-jet text-pure"
        type="search"
        name="search"
        autoComplete="off"
        defaultValue={props.value}
        onChange={event => props.onSearchTermChange(event.target.value)}
        placeholder={props.isCharSearch ? "Character Search..." : "Search..."}
    />
  );
}

const mapStateToProps = state => {
    return { ...state }
}

export default connect(mapStateToProps)(SearchBar)