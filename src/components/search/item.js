import React from 'react';
import { connect } from 'react-redux';
import ItemDetail from './item_detail';
import CharDetail from './char_detail';
import { Collapse } from 'reactstrap';
import fallbackIcon from '../../images/Mob15_Icon.png';
import * as actions from '../../state/actions';
import { actionTypes } from '../../state/actions';

class Item extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            item: this.props.item,
            itemDetails: {},
            isOpen: false,
            isLoading: false,
            attempt: 0
        };

        this.toggle = this.toggle.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    toggle() {
        this.setState({ isOpen: !this.state.isOpen});
    }

    handleClick(toggle=true, refresh=false) {
        let url = this.props.isCharSearch ? `/character/${this.state.item.ID}` : this.state.item.Url;
        if (this.props.isCharSearch && !this.state.itemDetails.Character) {
            refresh = true;
        } else if (!this.props.isCharSearch && !this.state.itemDetails.ID) {
            refresh = true;
        }
        
        if (!refresh) {
            if(toggle) this.toggle();
        } else {
            this.setState({
                isLoading: true
            }, () => {
                this.props.xivGet(url, this.props.isCharSearch ? actionTypes.XIVGET_CHARACTER : actionTypes.XIVGET_ITEM)
                .then((response) => {
                    if (this.state.attempt < 5 && this.props.isCharSearch && response.action.payload.data.Character === null) {
                        setTimeout(() => this.setState({attempt: this.state.attempt+1},this.handleClick), 500);
                    } else {
                        this.setState({
                            itemDetails: response.action.payload.data,
                            isLoading: false,
                            attempt: this.state.attempt - 1
                        }, toggle ? this.toggle : null);
                    }          
                })
                .catch((error) => {
                    this.setState({
                        isLoading: false
                    });
                    throw error;
                })
            })
        }
    }

    render() {
        const name = this.state.item.Name;
        const id = this.state.item.ID;
        const icon = this.props.isCharSearch ? this.state.item.Avatar : this.state.item.Icon === null ? fallbackIcon : `https://xivapi.com${this.state.item.Icon}`;
        const type = this.props.isCharSearch ? "Character" : this.state.item.UrlType;
        
        const loaderClassLeft = this.state.isLoading ? "loader-pulse-left ": "";
        const loaderClassRight = this.state.isLoading ? "loader-pulse-right ": "";

        return (
            <div className="item">
                <div className="item-top">
                    <div className="item-header" onClick={this.handleClick}>
                        <div className="loader">
                            <span className={loaderClassLeft + "loader-bg-left"}></span>
                            <span className={loaderClassRight + "loader-bg-right"}></span>
                        </div>
                        <img className="result-icon" src={icon} alt={`${this.state.item.Name} icon`}/>
                        <div className="result" id={id}>
                            <span className="name">{name}</span>
                            <span className="type">{type}</span>
                        </div>
                    </div>
                </div>
                <Collapse isOpen={this.state.isOpen} >
                    {this.props.isCharSearch
                        ? <CharDetail item={this.state.itemDetails} toggle={this.toggle} onClick={this.handleClick} />
                        : <ItemDetail item={this.state.itemDetails} toggle={this.toggle} onClick={this.handleClick} />
                    }
                </Collapse>
            </div>
        )
    };
};

const mapStateToProps = state => {
    return { ...state }
}

export default connect(mapStateToProps, actions)(Item)