import React, { Component } from "react";
import { connect } from 'react-redux';
import { debounce } from 'lodash';
import { Button,
    Card, 
    CardImg, 
    CardBody, 
    CardText, 
    CardTitle } from 'reactstrap';
import ReactHtmlParser from 'react-html-parser';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSync } from '@fortawesome/free-solid-svg-icons';

library.add(faSync);

class ItemDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: this.props.item,
            activeTab: "Profile"
        }

        this.handleClick = this.handleClick.bind(this);
        this.parseDescription = this.parseDescription.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.item !== prevState.item) {
            return ({ item: nextProps.item });
        } else {
            return null;
        }
    }

    handleClick() {
        if (!this.state.item.Description) {
            this.props.onClick(false, true);
        }
    }

    parseDescription() {
        if (this.state.item.Description) {
            const desc = this.state.item.Description;
            return desc.replace(/r?\nr?\n/g, "<br />");
        } else {
            return "Unable to retrieve data";
        }
    }

  render() {
    const currItem = this.state.item;
        const desc = this.parseDescription();
        if (currItem.ID) {
            const hasBanner = currItem.GamePatch.Banner ? true : false;
            const patchName = currItem.GamePatch.Name;
            const patchBanner = hasBanner ? currItem.GamePatch.Banner.replace("http://", "https://") : null;
            return (
                <Card className="item-detail bg-jet">
                {hasBanner ? 
                    <CardImg top src={patchBanner} alt={patchName} title={patchName} /> 
                : null}
                <CardBody>
                    <CardTitle>{patchName}</CardTitle>
                    <CardText>{ReactHtmlParser(desc)}</CardText>
                </CardBody>
                </Card>
            );
        } else {
            return (
                <Card className="item-detail bg-jet">
                    <CardBody className="detail-null">
                        <span>Unable to retrieve data</span>
                        <Button color="link" className="text-light" onClick={debounce(this.handleClick, 500)}>
                        <FontAwesomeIcon icon="sync" />
                        </Button>
                    </CardBody>
                </Card>
            )
        }
    }
};

const mapStateToProps = state => {
    return { ...state }
}

export default connect(mapStateToProps)(ItemDetail)