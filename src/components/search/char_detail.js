import React, { Component } from "react";
import { connect } from 'react-redux';
import { debounce } from 'lodash';
import { Button,
    Card,
    CardBody,
    Nav,
    NavItem,
    NavLink,
    TabContent,
    TabPane } from 'reactstrap';
import classnames from 'classnames';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSync } from '@fortawesome/free-solid-svg-icons';

library.add(faSync);

class CharDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: this.props.item,
            activeTab: "Profile"
        }

        this.handleClick = this.handleClick.bind(this);
        this.parseCharacterClasses = this.parseCharacterClasses.bind(this);
        this.parseRace = this.parseRace.bind(this);
        this.parseTown = this.parseTown.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.item !== prevState.item) {
            return ({ item: nextProps.item });
        } else {
            return null;
        }
    }

    handleClick() {
        if (!this.state.item.Description) {
            this.props.onClick(false, true);
        }
    }

    parseRace(raceNum) {
        switch (raceNum) {
            case 1:
                return "Hyur";
            case 2:
                return "Elezen";
            case 3:
                return "Lalafell";
            case 4:
                return "Miqo'te";
            case 5:
                return "Roegadyn";
            case 6:
                return "Au Ra";
            default:
                return "Undefined";
        }
    }

    parseTribe(tribeNum) {
        switch (tribeNum) {
            case 1:
                return "Midlander";
            case 2:
                return "Highlander";
            case 3:
                return "Wildwood";
            case 4:
                return "Duskwight";
            case 5:
                return "Plainsfolk";
            case 6:
                return "Dunesfolk";
            case 7:
                return "Seeker of the Sun";
            case 8:
                return "Keeper of the Moon";
            case 9:
                return "Sea Wolf";
            case 10:
                return "Hellsguard"
            case 11:
                return "Raen";
            case 12:
                return "Xaela";
            default:
                return "Undefined";
        }
    }

    parseTown(townNum) {
        switch (townNum) {
            case 1:
                return "Limsa Lominsa";
            case 2:
                return "Gridania";
            case 3: 
                return "Uldah";
            default:
                return "Undefined";
        }
    }

    parseCharacterClasses() {
    const classJobs = this.state.item.Character.ClassJobs;
    let tanks = [];
    let healers = [];
    let melee = [];
    let physrange = [];
    let magrange = [];
    let dol = [];
    let doh = [];
    
    const jobs = {
        DOW: {
            Tank: {
                DRK: {
                    class: "none",
                    classString: "Dark Knight",
                    jobString: "Dark Knight",
                    jobId: 32,
                    classId: 32,
                    level: classJobs["32_32"].Level,
                    expLevel: classJobs["32_32"].ExpLevel,
                    expLevelMax: classJobs["32_32"].ExpLevelMax,
                    expLevelTogo: classJobs["32_32"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/darkknight.png",
                    jobIcon: "https://xivapi.com/cj/1/darkknight.png"
                },
                PLD: {
                    class: "GLA",
                    classString: "Gladiator",
                    jobString: "Gladiator/Paladin",
                    jobId: 19,
                    classId: 1,
                    level: classJobs["1_19"].Level,
                    expLevel: classJobs["1_19"].ExpLevel,
                    expLevelMax: classJobs["1_19"].ExpLevelMax,
                    expLevelTogo: classJobs["1_19"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/gladiator.png",
                    jobIcon: "https://xivapi.com/cj/1/paladin.png"
                },
                WAR: {
                    class: "MRD",
                    classString: "Marauder",
                    jobString: "Marauder/Warrior",
                    jobId: 21,
                    classId: 3,
                    level: classJobs["3_21"].Level,
                    expLevel: classJobs["3_21"].ExpLevel,
                    expLevelMax: classJobs["3_21"].ExpLevelMax,
                    expLevelTogo: classJobs["3_21"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/marauder.png",
                    jobIcon: "https://xivapi.com/cj/1/warrior.png"  
                }
            },
            Healer: {
                AST: {
                    class: "none",
                    classString: "Astrologian",
                    jobString: "Astrologian",
                    jobId: 33,
                    classId: 33,
                    level: classJobs["33_33"].Level,
                    expLevel: classJobs["33_33"].ExpLevel,
                    expLevelMax: classJobs["33_33"].ExpLevelMax,
                    expLevelTogo: classJobs["33_33"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/astrologian.png",
                    jobIcon: "https://xivapi.com/cj/1/astrologian.png"
                },
                SCH: {
                    class: "ARC",
                    classString: "Arcanist",
                    jobString: "Arcanist/Scholar",
                    jobId: 28,
                    classId: 26,
                    level: classJobs["26_28"].Level,
                    expLevel: classJobs["26_28"].ExpLevel,
                    expLevelMax: classJobs["26_28"].ExpLevelMax,
                    expLevelTogo: classJobs["26_28"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/arcanist.png",
                    jobIcon: "https://xivapi.com/cj/1/summoner.png"
                },
                WHM: {
                    class: "CNJ",
                    classString: "Conjurer",
                    jobString: "Conjurer/White Mage", 
                    jobId: 24,
                    classId: 6,
                    level: classJobs["6_24"].Level,
                    expLevel: classJobs["6_24"].ExpLevel,
                    expLevelMax: classJobs["6_24"].ExpLevelMax,
                    expLevelTogo: classJobs["6_24"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/conjurer.png",
                    jobIcon: "https://xivapi.com/cj/1/whitemage.png"
                }
            },
            MeleeDPS: {
                DRG: {
                    class: "LNC",
                    classString: "Lancer",
                    jobString: "Lancer/Dragoon",
                    jobId: 22,
                    classId: 4,
                    level: classJobs["4_22"].Level,
                    expLevel: classJobs["4_22"].ExpLevel,
                    expLevelMax: classJobs["4_22"].ExpLevelMax,
                    expLevelTogo: classJobs["4_22"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/lancer.png",
                    jobIcon: "https://xivapi.com/cj/1/dragoon.png"
                },
                MNK: {
                    class: "PGL",
                    classString: "Pugilist",
                    jobString: "Pugilist/Monk",
                    jobId: 20,
                    classId: 2,
                    level: classJobs["2_20"].Level,
                    expLevel: classJobs["2_20"].ExpLevel,
                    expLevelMax: classJobs["2_20"].ExpLevelMax,
                    expLevelTogo: classJobs["2_20"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/pugilist.png",
                    jobIcon: "https://xivapi.com/cj/1/monk.png"
                },
                NIN: {
                    class: "ROG",
                    classString: "Rogue",
                    jobString: "Rogue/Ninja",
                    jobId: 30,
                    classId: 29,
                    level: classJobs["29_30"].Level,
                    expLevel: classJobs["29_30"].ExpLevel,
                    expLevelMax: classJobs["29_30"].ExpLevelMax,
                    expLevelTogo: classJobs["29_30"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/rogue.png",
                    jobIcon: "https://xivapi.com/cj/1/ninja.png"
                },
                SAM: {
                    class: "none",
                    classString: "Samurai",
                    jobString: "Samurai",
                    jobId: 34,
                    classId: 34,
                    level: classJobs["34_34"].Level,
                    expLevel: classJobs["34_34"].ExpLevel,
                    expLevelMax: classJobs["34_34"].ExpLevelMax,
                    expLevelTogo: classJobs["34_34"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/samurai.png",
                    jobIcon: "https://xivapi.com/cj/1/samurai.png"
                }
            },
            PhysRangeDPS: {
                BRD: {
                    class: "ARC",
                    classString: "Archer",
                    jobString: "Archer/Bard",
                    jobId: 23,
                    classId: 5,
                    level: classJobs["5_23"].Level,
                    expLevel: classJobs["5_23"].ExpLevel,
                    expLevelMax: classJobs["5_23"].ExpLevelMax,
                    expLevelTogo: classJobs["5_23"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/archer.png",
                    jobIcon: "https://xivapi.com/cj/1/bard.png"
                },
                MCH: {
                    class: "none",
                    classString: "Machinist",
                    jobString: "Machinist",
                    jobId: 31,
                    classId: 31,
                    level: classJobs["31_31"].Level,
                    expLevel: classJobs["31_31"].ExpLevel,
                    expLevelMax: classJobs["31_31"].ExpLevelMax,
                    expLevelTogo: classJobs["31_31"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/machinist.png",
                    jobIcon: "https://xivapi.com/cj/1/machinist.png"
                }
            },
            MagRangeDPS: {
                BLM: {
                    class: "THM",
                    classString: "Thaumaturge",
                    jobString: "Thaumaturge/Black Mage",
                    jobId: 25,
                    classId: 7,
                    level: classJobs["7_25"].Level,
                    expLevel: classJobs["7_25"].ExpLevel,
                    expLevelMax: classJobs["7_25"].ExpLevelMax,
                    expLevelTogo: classJobs["7_25"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/blackmage.png",
                    jobIcon: "https://xivapi.com/cj/1/thaumaturge.png"
                },
                RDM: {
                    class: "none",
                    classString: "Red Mage",
                    jobString: "Red Mage",
                    jobId: 35,
                    classId: 35,
                    level: classJobs["35_35"].Level,
                    expLevel: classJobs["35_35"].ExpLevel,
                    expLevelMax: classJobs["35_35"].ExpLevelMax,
                    expLevelTogo: classJobs["35_35"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/redmage.png",
                    jobIcon: "https://xivapi.com/cj/1/redmage.png"
                },
                SMN: {
                    class: "ARC",
                    classString: "Arcanist",
                    jobString: "Arcanist/Summoner",
                    jobId: 27,
                    classId: 26,
                    level: classJobs["26_27"].Level,
                    expLevel: classJobs["26_27"].ExpLevel,
                    expLevelMax: classJobs["26_27"].ExpLevelMax,
                    expLevelTogo: classJobs["26_27"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/arcanist.png",
                    jobIcon: "https://xivapi.com/cj/1/summoner.png"
                }
            },
            Limited: {
                BLU: {
                    class: "none",
                    classString: "Blue Mage",
                    jobString: "Blue Mage",
                    jobId: 36,
                    classId: 36,
                    level: classJobs["36_36"].Level,
                    expLevel: classJobs["36_36"].ExpLevel,
                    expLevelMax: classJobs["36_36"].ExpLevelMax,
                    expLevelTogo: classJobs["36_36"].ExpLevelTogo,
                    classIcon: "https://xivapi.com/cj/1/bluemage.png",
                    jobIcon: "https://xivapi.com/cj/1/bluemage.png"
                }
            }
        },
        DOL: {
            BTN: {
                class: "BTN",
                classString: "Botanist",
                jobString: "Botanist",
                jobId: 17,
                classId: 17,
                level: classJobs["17_17"].Level,
                expLevel: classJobs["17_17"].ExpLevel,
                expLevelMax: classJobs["17_17"].ExpLevelMax,
                expLevelTogo: classJobs["17_17"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/botanist.png",
                jobIcon: "https://xivapi.com/cj/1/botanist.png"
            },
            FSH: {
                class: "FSH",
                classString: "Fisher",
                jobString: "Fisher",
                jobId: 18,
                classId: 18,
                level: classJobs["18_18"].Level,
                expLevel: classJobs["18_18"].ExpLevel,
                expLevelMax: classJobs["18_18"].ExpLevelMax,
                expLevelTogo: classJobs["18_18"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/fisher.png",
                jobIcon: "https://xivapi.com/cj/1/fisher.png"
            },
            MIN: {
                class: "MIN",
                classstring: "Miner",
                jobString: "Miner",
                jobId: 16,
                classId: 16,
                level: classJobs["16_16"].Level,
                expLevel: classJobs["16_16"].ExpLevel,
                expLevelMax: classJobs["16_16"].ExpLevelMax,
                expLevelTogo: classJobs["16_16"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/miner.png",
                jobIcon: "https://xivapi.com/cj/1/miner.png"
            }
        },
        DOH: {
            ALC: {
                class: "ALC",
                classstring: "Alchemist",
                jobString: "Alchemist",
                jobId: 14,
                classId: 14,
                level: classJobs["14_14"].Level,
                expLevel: classJobs["14_14"].ExpLevel,
                expLevelMax: classJobs["14_14"].ExpLevelMax,
                expLevelTogo: classJobs["14_14"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/alchemist.png",
                jobIcon: "https://xivapi.com/cj/1/alchemist.png"
            },
            ARM: {
                class: "ARM",
                classString: "Armorer",
                jobString: "Armorer",
                jobId: 10,
                classId: 10,
                level: classJobs["10_10"].Level,
                expLevel: classJobs["10_10"].ExpLevel,
                expLevelMax: classJobs["10_10"].ExpLevelMax,
                expLevelTogo: classJobs["10_10"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/armorer.png",
                jobIcon: "https://xivapi.com/cj/1/armorer.png"
            },
            BSM: {
                class: "BSM",
                classString: "Blacksmith",
                jobString: "Blacksmith",
                jobId: 9,
                classId: 9,
                level: classJobs["9_9"].Level,
                expLevel: classJobs["9_9"].ExpLevel,
                expLevelMax: classJobs["9_9"].ExpLevelMax,
                expLevelTogo: classJobs["9_9"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/blacksmith.png",
                jobIcon: "https://xivapi.com/cj/1/blacksmith.png"
            },
            CRP: {
                class: "CRP",
                classString: "Carpenter",
                jobString: "Carpenter",
                jobId: 8,
                classId: 8,
                level: classJobs["8_8"].Level,
                expLevel: classJobs["8_8"].ExpLevel,
                expLevelMax: classJobs["8_8"].ExpLevelMax,
                expLevelTogo: classJobs["8_8"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/carpenter.png",
                jobIcon: "https://xivapi.com/cj/1/carpenter.png"
            },
            CUL: {
                class: "CUL",
                classString: "Culinarian",
                jobString: "Culinarian",
                jobId: 15,
                classId: 15,
                level: classJobs["15_15"].Level,
                expLevel: classJobs["15_15"].ExpLevel,
                expLevelMax: classJobs["15_15"].ExpLevelMax,
                expLevelTogo: classJobs["15_15"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/culinarian.png",
                jobIcon: "https://xivapi.com/cj/1/culinarian.png"
            },
            GSM: {
                class: "GSM",
                classString: "Goldsmith",
                jobString: "Goldsmith",
                jobId: 11,
                classId: 11,
                level: classJobs["11_11"].Level,
                expLevel: classJobs["11_11"].ExpLevel,
                expLevelMax: classJobs["11_11"].ExpLevelMax,
                expLevelTogo: classJobs["11_11"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/goldsmith.png",
                jobIcon: "https://xivapi.com/cj/1/goldsmith.png"
            },  
            LTW: {
                class: "LTW",
                classString: "Leatherworker",
                jobString: "Leatherworker",
                jobId: 12,
                classId: 12,
                level: classJobs["12_12"].Level,
                expLevel: classJobs["12_12"].ExpLevel,
                expLevelMax: classJobs["12_12"].ExpLevelMax,
                expLevelTogo: classJobs["12_12"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/leatherworker.png",
                jobIcon: "https://xivapi.com/cj/1/leatherworker.png"
            },
            WVR: {
                class: "WVR",
                classString: "Weaver",
                jobString: "Weaver",
                jobId: 13,
                classId: 13,
                level: classJobs["13_13"].Level,
                expLevel: classJobs["13_13"].ExpLevel,
                expLevelMax: classJobs["13_13"].ExpLevelMax,
                expLevelTogo: classJobs["13_13"].ExpLevelTogo,
                classIcon: "https://xivapi.com/cj/1/weaver.png",
                jobIcon: "https://xivapi.com/cj/1/weaver.png"
            }
        }
    }

    for (let each in jobs.DOW) {
        switch (each) {
            case "Tank":
                for (let charClass in jobs.DOW[each]) {
                    let job = jobs.DOW[each][charClass]
                    let string = job.level >= 50 ? job.jobString : job.classString;
                    let unique = job.classString.split(" ").join("_").toLowerCase();
                    let icon = job.level >= 50 ? job.jobIcon : job.classIcon;
                        tanks.push(
                            <div className={`char-class class-${unique}`} id={unique} key={unique}>
                                <img className="char-class-icon" src={icon} alt={string} title={string}/>
                                {job.level > 0 ? job.level : "-"}
                            </div>
                        );
                    }
                break;         
            case "Healer":
                for (let charClass in jobs.DOW[each]) {
                    let job = jobs.DOW[each][charClass];
                    let string = job.level >= 50 ? job.jobString : job.classString;
                    let icon = job.level >= 50 ? job.jobIcon : job.classIcon;
                    let unique = job.classString.split(" ").join("_").toLowerCase();
                    healers.push(
                        <div className={`char-class class-${unique}`} id={unique} key={unique}>
                            <img className="char-class-icon" src={icon} alt={string} title={string}/>
                            {job.level > 0 ? job.level : "-"}
                        </div>
                    );
                }  
                break;
            case "MeleeDPS":
                for (let charClass in jobs.DOW[each]) {
                    let job = jobs.DOW[each][charClass];
                    let string = job.level >= 50 ? job.jobString : job.classString;
                    let icon = job.level >= 50 ? job.jobIcon : job.classIcon;
                    let unique = job.classString.split(" ").join("_").toLowerCase();
                    melee.push(
                        <div className={`char-class class-${unique}`} id={unique} key={unique}>
                            <img className="char-class-icon" src={icon} alt={string} title={string}/>
                            {job.level > 0 ? job.level : "-"}
                        </div>
                    );
                }  
                break;
            case "PhysRangeDPS":
                for (let charClass in jobs.DOW[each]) {
                    let job = jobs.DOW[each][charClass];
                    let string = job.level >= 50 ? job.jobString : job.classString;
                    let icon = job.level >= 50 ? job.jobIcon : job.classIcon;
                    let unique = job.classString.split(" ").join("_").toLowerCase();
                    physrange.push(
                        <div className={`char-class class-${unique}`} id={unique} key={unique}>
                            <img className="char-class-icon" src={icon} alt={string} title={string}/>
                            {job.level > 0 ? job.level : "-"}
                        </div>
                    );
                }  
                break;
            case "MagRangeDPS":
                for (let charClass in jobs.DOW[each]) {
                    let job = jobs.DOW[each][charClass];
                    let string = job.level >= 50 ? job.jobString : job.classString;
                    let icon = job.level >= 50 ? job.jobIcon : job.classIcon;
                    let unique = job.classString.split(" ").join("_").toLowerCase();
                    magrange.push(
                        <div className={`char-class class-${unique}`} id={unique} key={unique}>
                            <img className="char-class-icon" src={icon} alt={string} title={string}/>
                            {job.level > 0 ? job.level : "-"}
                        </div>
                    );
                }  
                break;
            default: 
                break;
        } 
    }

    for (let each in jobs.DOL) {
        let job = jobs.DOL[each];
        let unique = job.jobString.split(" ").join("_").toLowerCase();
        dol.push(
            <div className={`char-class class-${unique}`} id={unique} key={unique}>
                <img className="char-class-icon" src={job.jobIcon} alt={job.jobString} title={job.jobString}/>
                {job.level > 0 ? job.level : "-"}
            </div>
        );
    }

    for (let each in jobs.DOH) {
        let job = jobs.DOH[each];
        let unique = job.jobString.split(" ").join("_").toLowerCase();
        doh.push(
            <div className={`char-class class-${unique}`} id={unique} key={unique}>
                <img className="char-class-icon" src={job.jobIcon} alt={job.jobString} title={job.jobString}/>
                {job.level > 0 ? job.level : "-"}
            </div>
        );
    }

    return(
        <div className="character-classes">
            <div className="character-classes-dow">
                <div className="character-classes-dow-tanks">
                    <div className="character-classes-label">Tank</div>
                    {tanks}
                </div>
                <div className="character-classes-dow-healers">
                    <div className="character-classes-label">Healer</div>
                    {healers}
                </div>
                <div className="character-classes-dow-dps">
                    <div className="character-classes-label">DPS</div> 
                    <div className="character-classes-dow-dps-melee">
                        {melee}
                    </div>
                    <div className="character-classes-dow-dps-physrange">
                        {physrange}
                    </div>
                    <div className="character-classes-dow-dps-magrange">
                        {magrange}
                    </div>
                </div>
            </div>
            <div className="character-classes-dol">
                <div className="character-classes-dol-label">Disciple of Land</div>
                {dol}
            </div>
            <div className="character-classes-doh">
                <div className="character-classes-doh-label">Disciple of Hand</div>
                {doh}
            </div>
        </div>
    );
    
  }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

  render() {
    const currItem = this.state.item;
    const charClasses = this.state.item.Character ? this.parseCharacterClasses() : null;
    if (currItem.Character) {
        return(
            <Card className="item-detail bg-jet">
                <CardBody>
                    <div className="character-grid">
                        <img className="character-portrait" src={currItem.Character.Portrait} alt={`${currItem.Character.Name} portrait`}/>
                        <div className="character-title">
                            <h4 className="character-name">{currItem.Character.Name}</h4>
                            <h6 className="character-server">
                                {currItem.Character.Server}
                                <Button color="link" className="item-refresh text-light" onClick={debounce(this.handleClick, 500)}>
                                    <FontAwesomeIcon icon="sync" />
                                </Button>
                            </h6>
                        </div>
                        <Nav className="character-tabs" tabs>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === 'Profile' })}
                                    onClick={() => { this.toggle('Profile'); }}
                                >
                                    Profile
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === 'ClassJob' })}
                                    onClick={() => { this.toggle('ClassJob'); }}
                                >
                                    Class/Job
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    className={classnames({ active: this.state.activeTab === 'MountsMinions' })}
                                    onClick={() => { this.toggle('MountsMinions'); }}
                                >
                                    Mounts/Minions
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <TabContent className="character-content" activeTab={this.state.activeTab}>
                            <TabPane tabId="Profile">
                                <div className="character-origin">
                                    <div className="character-race">
                                        <span className="char-label">Race: </span>
                                        <span className="char-profVal">{this.parseRace(currItem.Character.Race)}</span>
                                    </div>
                                    <div className="character-tribe">
                                        <span className="char-label">Clan: </span>
                                        <span className="char-profVal">{this.parseTribe(currItem.Character.Tribe)}</span>
                                    </div>
                                    <div className="character-gender">
                                        <span className="char-label">Gender: </span>
                                        <span className="char-profVal">{currItem.Character.Gender === 1 ? "Male" : "Female"}</span>
                                    </div>
                                    <div className="character-nameday">
                                        <span className="char-label">Nameday: </span>
                                        <span className="char-profVal">{currItem.Character.Nameday}</span>
                                    </div>
                                    <div className="character-town">
                                        <span className="char-label">City-state: </span>
                                        <span className="char-profVal">{this.parseTown(currItem.Character.Town)}</span>
                                    </div>
                                </div>
                            </TabPane>
                            <TabPane tabId="ClassJob">
                                {charClasses}
                            </TabPane>
                            <TabPane tabId="MountsMinions">
                                Mounts/Minions
                            </TabPane>
                        </TabContent>
                    </div>
                </CardBody>
            </Card>
        );
    } else {
        return (
            <Card className="char-detail bg-jet">
                <CardBody className="detail-null">
                    <span>Unable to retrieve data</span>
                    <Button color="link" className="text-light" onClick={debounce(this.handleClick, 500)}>
                        <FontAwesomeIcon icon="sync" />
                    </Button>
                </CardBody>
            </Card>
        );
    }
  }
};

const mapStateToProps = state => {
    return { ...state }
}

export default connect(mapStateToProps)(CharDetail)