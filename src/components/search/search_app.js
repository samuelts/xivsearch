import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux'
import { Button } from 'reactstrap';

import * as actions from '../../state/actions';

class SearchApp extends React.Component {
    constructor(props) {
        super(props);

        // Function bindings
        this.prevPage = this.prevPage.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.onLimitSelect = this.onLimitSelect.bind(this);
    }

    onLimitSelect(e) {
        this.props.onLimitSelect(e.target.value)
        .then(() => {
            this.props.clearList();
            let api_url = actions.buildUrl();
            this.props.xivSearch(api_url);
        });
        }

    // Page Navigation
    //=====================================
    prevPage() {
        this.props.clearList();
        const prevPage = this.props.pagination.PagePrev;
        let api_url = actions.buildUrl(prevPage);
        this.props.xivSearch(api_url);
    }

    nextPage() {
        this.props.clearList();
        const nextPage = this.props.pagination.PageNext;
        let api_url = actions.buildUrl(nextPage);
        this.props.xivSearch(api_url);
    }

    // Render
    //=====================================
    render() {
        const { isCharSearch, limit, itemList, pagination } = this.props;
        const prevPage = isCharSearch ? pagination.PagePrevious : pagination.PagePrev;
        const nextPage = pagination.PageNext;
        const prevVis = prevPage ? {visibility: "visible"} : {visibility: "hidden"};
        const nextVis = nextPage ? {visibility: "visible"} : {visibility: "hidden"};
        const limitEnabled = isCharSearch ? "disabled" : null;
        const limitCor = isCharSearch ? 50 : limit;

        return (
            <div className="page-wrap" id="search-wrap">
                <div className="lds-css ng-scope" style={this.props.searchLoading ? {display: "block"} : {display: "none"}}>
                    <div className="lds-eclipse">
                        <div></div>
                    </div>
                </div>
                <div id="item-list">
                    {itemList}
                </div>
                <div className="page-controls">
                    <Button color="dalamud" id="prev-page" style={prevVis} onClick={_.debounce(this.prevPage, 300)}>Prev</Button>
                    <div className={isCharSearch ? "disabled" : null} id="curr-page">
                        <div className={isCharSearch ? "text-goobbue" : "text-light"}>Results per page: </div>
                        <select 
                            className={isCharSearch ? "text-goobbue bg-secondary" : "text-light bg-dalamud"}
                            id="limit" 
                            onChange={this.onLimitSelect} 
                            value={limitCor} 
                            disabled={limitEnabled}
                        >
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        {pagination.Page ? ` Page: ${pagination.Page} / ${pagination.PageTotal}` : null}
                    </div>
                    <Button color="dalamud" id="next-page" style={nextVis} onClick={_.debounce(this.nextPage, 300)}>Next</Button>
                </div>
            </div>
        )
    };
}

const mapStateToProps = state => {
    return { ...state }
}

export default connect(mapStateToProps, actions)(SearchApp)